"use strict";

const Post = use("App/Models/Post");
const { validateAll } = use("Validator");
const Antl = use("Antl");
const Env = use("Env");

const validation_msg = use("App/Helpers/validator_message");

class PostController {
  async index({ request, response }) {
    const all = request.all();
    const page = all.page || 1;
    const search = all.q || "";
    const where = all.where || 1;
    let isSearch = true;

    let posts = null;

    if (search == null || search == "" ){

      posts = await Post.query()
        .has("author")
        .with("author", builder => {
          builder.setVisible(["username", "email"]);
        })
        .orderBy("created_at", "desc")
        .paginate(page, Env.get("POST_PER_PAGE", 5));

    } else if ( where == 1 ){
      posts = await Post.query()
        .where(function () {
          this
            .where('title', 'LIKE', `%${search}%` )
            .orWhere('body','LIKE', `%${search}%` )
        })
        .has("author")
        .with("author", builder => {
          builder.setVisible(["username", "email"]);
        })
        .orderBy("created_at", "desc")
        .paginate(page, Env.get("POST_PER_PAGE", 5));
    } else if (where == 2 ){
      posts = await Post.query()
        .where('title', 'LIKE', `%${search}%` )
        .has("author")
        .with("author", builder => {
          builder.setVisible(["username", "email"]);
        })
        .orderBy("created_at", "desc")
        .paginate(page, Env.get("POST_PER_PAGE", 5));
    } else {
      posts = await Post.query()
        .where('body', 'LIKE', `%${search}%` )
        .has("author")
        .with("author", builder => {
          builder.setVisible(["username", "email"]);
        })
        .orderBy("created_at", "desc")
        .paginate(page, Env.get("POST_PER_PAGE", 5));
    }

    return response.status(200).json({ message: "OK", posts: posts.toJSON() });
  }

  create({ view }) {
    return view.render("posts.create");
  }

  async store({ request, response, locale, auth }) {
    const data = request.only(["title", "body"]);

    /**
     * Validating our data.
     *
     * ref: http://adonisjs.com/docs/4.0/validator
     */
    const validation = await validateAll(
      data,
      {
        title: "required",
        body: "required"
      },
      validation_msg.getMessages(locale)
    );

    /**
     * If validation fails, early returns with validation message.
     */
    if (validation.fails()) {
      return response.status(401).json({ error: validation.messages() });
    }

    /**
     * Creating a new post into the database.
     *
     * ref: http://adonisjs.com/docs/4.0/lucid#_create
     */
    data.user_id = auth.user.id;

    let newPost = await Post.create(data);

    return response.status(201).json({ message: "OK", newPost });
  }

  async edit({ params, view }) {
    /**
     * Finding the post.
     *
     * ref: http://adonisjs.com/docs/4.0/lucid#_findorfail
     */
    const post = await Post.findOrFail(params.id);

    return view.render("posts.edit", { post: post.toJSON() });
  }

  async update({ params, session, request, response }) {
    /**
     * Getting needed parameters.
     *
     * ref: http://adonisjs.com/docs/4.0/request#_only
     */
    const data = request.only(["title", "body"]);

    /**
     * Validating our data.
     *
     * ref: http://adonisjs.com/docs/4.0/validator
     */
    const validation = await validate(data, {
      title: "required",
      body: "required"
    });

    /**
     * If validation fails, early returns with validation message.
     */
    if (validation.fails()) {
      session.withErrors(validation.messages()).flashAll();

      return response.redirect("back");
    }

    /**
     * Finding the post and updating fields on it
     * before saving it to the database.
     *
     * ref: http://adonisjs.com/docs/4.0/lucid#_inserts_updates
     */
    const post = await Post.findOrFail(params.id);
    post.merge(data);
    await post.save();

    return response.redirect("/");
  }

  async delete({ params, response }) {
    /**
     * Finding the post and deleting it
     *
     * ref: http://adonisjs.com/docs/4.0/lucid#_deletes
     */
    const post = await Post.findOrFail(params.id);
    await post.delete();

    return response.redirect("/");
  }
}

module.exports = PostController;
