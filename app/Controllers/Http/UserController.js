"use strict";

const User = use("App/Models/User");
const { validateAll } = use("Validator");
const Antl = use("Antl");

const validation_msg = use("App/Helpers/validator_message");

class UserController {
  create({ view }) {
    /**
     * Render the view 'user.create'.
     *
     * ref: http://adonisjs.com/docs/4.0/views
     */
    return view.render("user.create");
  }

  async store({ session, request, response, locale }) {
    /**
     * Getting needed parameters.
     *
     * ref: http://adonisjs.com/docs/4.0/request#_only
     */
    const data = request.only([
      "username",
      "email",
      "password",
      "password_confirmation"
    ]);

    /**
     * Validating our data.
     *
     * ref: http://adonisjs.com/docs/4.0/validator
     */
    const validation = await validateAll(
      data,
      {
        username: "required|unique:users",
        email: "required|email|unique:users",
        password: "required",
        password_confirmation: "required_if:password|same:password"
      },
      validation_msg.getMessages(locale)
    );

    if (validation.fails()) {
      return response.status(400).json({ error: validation.messages() });
    }

    // Deleting the confirmation field since we don't
    // want to save it
    delete data.password_confirmation;

    /**
     * Creating a new user into the database.
     *
     * ref: http://adonisjs.com/docs/4.0/lucid#_create
     */
    let newUser = await User.create(data);

    console.log(newUser.roles());

    await newUser.roles().attach([2]);

    delete newUser.confirmation_string; //Lets detelete this information
    delete newUser.password; //Lets detelete this information

    return response.status(200).json({ message: "OK", user: newUser });
  }

  async getOne({ session, request, response, params }) {
    let user = await User.find(params.id);
    let userRoles = await user.getRoles();
    console.log(userRoles);
    return response.status(200).json({ message: "OK", user });
  }
}

module.exports = UserController;
