"use strict";
const Antl = use("Antl");
const User = use("App/Models/User");

class SessionController {
  create({ view }) {
    /**
     * Render the view 'sessions.create'.
     *
     * ref: http://adonisjs.com/docs/4.0/views
     */
    return view.render("session.create");
  }

  /**
   * Store a session.
   */
  async store({ auth, request, response, session, locale }) {
    /**
     * Getting needed parameters.
     *
     * ref: http://adonisjs.com/docs/4.0/request#_all
     */
    const { email, password } = request.all();
    let logged;
    let user;
    let roles;
    let newUser;

    try {
      logged = await auth.attempt(email, password, true);
    } catch (e) {
      return response.status(400).json({
        error: Antl.forLocale(locale).formatMessage("messages.login_error"),
        err: e
      });
    }

    if (logged) {
      user = await User.findBy("email", email);
      roles = await user.getRoles();
      newUser = {
        id: user.id,
        username: user.username,
        email: user.email,
        roles: roles,
        created_at: user.created_at
      };

      delete user.is_confirmed;
      delete user.is_enabled;
      delete user.confirmation_string;
    }

    return response.json(await auth.generate(newUser, true));
    //return response.json(newUser);
  }

  async delete({ auth, response }) {
    /**
     * Logout the user.
     *
     * ref: http://adonisjs.com/docs/4.0/authentication#_logout
     */
    await auth.logout();

    return response.redirect("/");
  }
}

module.exports = SessionController;
