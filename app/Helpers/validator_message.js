const Antl = use("Antl");

module.exports = {
  getMessages: function(locale) {
    locale = locale.toLowerCase();
    switch (locale) {
      case "es":
        return this.messages_es;
      default:
        return this.messages_en;
    }
  },
  messages_en: {
    required: "The field {{ field }} is required"
  },
  messages_es: {
    required: `El campo {{ field }} es obligatorio`,
    "username.required": `Favor ingrese un Nombre de Usuario`,
    "email.required": "Ingresa un correo válido",
    email: "El formato del correo no es correcto"
  }
};
