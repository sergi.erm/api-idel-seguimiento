"use strict";

const Model = use("Model");

class User extends Model {
  static get primaryKey() {
    return "id";
  }
  static get incrementing() {
    return false;
  }

  static boot() {
    super.boot();

    /**
     * A hook to bash the user password before saving
     * it to the database.
     *
     * Look at `app/Models/Hooks/User.js` file to
     * check the hashPassword method
     */
    this.addHook("beforeCreate", "User.hashPassword");
    this.addHook("beforeCreate", "Common.generateUuid");
    this.addHook("beforeCreate", "Common.confirmationString");
  }

  static get traits() {
    return [
      "@provider:Adonis/Acl/HasPermission",
      "@provider:Adonis/Acl/HasRole"
    ];
  }

  static get hidden() {
    return ["password"];
  }

  role() {
    return this.belongsToMany("App/Models/Role");
  }
}

module.exports = User;
