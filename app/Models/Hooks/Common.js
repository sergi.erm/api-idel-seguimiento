"use strict";

const uuidV4 = require("uuid/v4");
const auth = use("Auth");

const CommonHook = (module.exports = {});

CommonHook.generateUuid = async dataInstance => {
  dataInstance.id = await uuidV4();
};

CommonHook.confirmationString = async dataInstance => {
  let confirmation = await uuidV4();
  confirmation += await uuidV4();
  confirmation = confirmation.replace(/-/g, "");

  dataInstance.confirmation_string = confirmation.substring(0, 50);
};
