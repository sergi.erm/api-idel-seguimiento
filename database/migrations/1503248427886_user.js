"use strict";

const Schema = use("Schema");

class UserSchema extends Schema {
  up() {
    this.create("users", table => {
      table
        .uuid("id")
        .notNullable()
        .unique();
      table.timestamps();
      table
        .string("username", 80)
        .notNullable()
        .unique();
      table
        .string("email", 254)
        .notNullable()
        .unique();
      table.string("password", 60).notNullable();
      table
        .boolean("is_confirmed")
        .defaultTo(false)
        .comment("This should be confirmed by email");
      table
        .boolean("is_enabled")
        .defaultTo(true)
        .comment("The user is enabled by default");
      table
        .string("confirmation_string", 50)
        .notNullable()
        .comment("We need to send this string via email");
    });
  }

  down() {
    this.drop("users");
  }
}

module.exports = UserSchema;
