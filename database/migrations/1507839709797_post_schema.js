"use strict";

const Schema = use("Schema");

class PostSchema extends Schema {
  up() {
    this.create("posts", table => {
      table.increments();
      table.timestamps();
      table.string("title");
      table.text("body");
      table.uuid("user_id").notNullable();
    });
  }

  down() {
    this.drop("posts");
  }
}

module.exports = PostSchema;
